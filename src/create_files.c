// This is free and unencumbered software released into the public domain.
// For more information, see UNLICENSE.

#include "common/time.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const unsigned int NUM_TRIALS = 100;

// Note: The maximum number of files in a folder for different file systems:
//  - 65534 (FAT32)
//  - 4294967295 (NTFS, ext4)
static const int NUM_FILES = (1 << 12);

static double my_log2(double x) {
  static const double LOG2SCALE = 1.442695040888963;  // 1.0 / log(2.0);
  return log(x) * LOG2SCALE;
}

static int num_hex_chars(int max_int) {
  int num_bits = (int)ceil(my_log2((double)max_int));
  return (num_bits + 3) / 4;
}

static char path_separator() {
#if defined(_WIN32)
  return '\\';
#else
  return '/';
#endif
}

static void to_hex(int x, char* str, const int str_len) {
  static const char TOHEX[16] = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
  };

  str += str_len - 1;
  for (int i = 0; i < str_len; ++i) {
    *str-- = TOHEX[x & 15];
    x = x >> 4;
  }
}

static void create_file(const char* file_name) {
  static const char FILE_DATA[32] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    26, 27, 28, 29, 30, 31
  };

  FILE *f = fopen(file_name, "wb");
  if (!f) {
    fprintf(stderr, "*** Unable to create file \"%s\"\n", file_name);
    exit(1);
  }
  fwrite(FILE_DATA, 1, sizeof(FILE_DATA), f);
  fclose(f);
}

static void delete_file(const char* file_name) {
  remove(file_name);
}

int main(int argc, const char** argv) {
  // Create a path string.
  char* root_path = "./tmp";
  int hex_len = num_hex_chars(NUM_FILES - 1);
  size_t root_path_len = strlen(root_path);
  size_t path_len = root_path_len + 1 + hex_len;
  char* file_name = (char*)malloc(path_len + 1);
  if (!file_name) {
    fprintf(stderr, "*** Out of memory!\n");
    exit(1);
  }
  strncpy(file_name, root_path, root_path_len);
  file_name[root_path_len] = path_separator();
  file_name[path_len] = 0;

  int trial_num = 0;
  while (trial_num++ < NUM_TRIALS) {
    const double t0 = get_time();

    for (int file_no = 0; file_no < NUM_FILES; ++file_no) {
      // Construct the file name for this file.
      to_hex(file_no, &file_name[root_path_len + 1], hex_len);

      // Create the file.
      create_file(file_name);
    }

    for (int file_no = 0; file_no < NUM_FILES; ++file_no) {
      // Construct the file name for this file.
      to_hex(file_no, &file_name[root_path_len + 1], hex_len);

      // Delete the file.
      delete_file(file_name);
    }

    double dt = (get_time() - t0) / NUM_FILES;
    printf("%g\n", dt);
    fflush(stdout);
  }

  free((void*)file_name);

  return 0;
}

