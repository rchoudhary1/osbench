// This is free and unencumbered software released into the public domain.
// For more information, see UNLICENSE.

#include "common/time.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static const unsigned int NUM_TRIALS = 100;
static const int NUM_PROCESSES = (1 << 12);

int main() {
  int trial_num = 0;
  while (trial_num++ < NUM_TRIALS) {
    pid_t processes[NUM_PROCESSES];
    const double t0 = get_time();

    // Create all the processes.
    for (int i = 0; i < NUM_PROCESSES; ++i) {
      pid_t pid = fork();
      if (pid == 0) {
        exit(0);
      } else if (pid > 0) {
        processes[i] = pid;
      } else {
        fprintf(stderr, "*** Unable to create process no. %d\n", i);
        exit(1);
      }
    }

    // Wait for all child processes to terminate.
    for (int i = 0; i < NUM_PROCESSES; ++i) {
      waitpid(processes[i], (int*)0, 0);
    }

    double dt = (get_time() - t0) / NUM_PROCESSES;
    printf("%g\n", dt);
    fflush(stdout);
  }

  return 0;
}

