// This is free and unencumbered software released into the public domain.
// For more information, see UNLICENSE.

#include "common/time.h"

#include <stdio.h>
#include <stdlib.h>

static const unsigned int NUM_TRIALS = 100;
#define NUM_ALLOCS (1 << 20)

static void* s_addresses[NUM_ALLOCS];

int main(int argc, const char** argv) {
  int trial_num = 0;
  while (trial_num++ < NUM_TRIALS) {
    const double t0 = get_time();

    for (int i = 0; i < NUM_ALLOCS; ++i) {
      const size_t memory_size = ((i % 32) + 1) * 4;
      s_addresses[i] = malloc(memory_size);
      ((char*)s_addresses[i])[0] = 1;
    }

    for (int i = 0; i < NUM_ALLOCS; ++i) {
      free(s_addresses[i]);
    }

    double dt = (get_time() - t0) / NUM_ALLOCS;
    printf("%g\n", dt);
    fflush(stdout);
  }

  return 0;
}

